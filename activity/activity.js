//Q: - What directive is used by Node.js in loading the modules it needs?
     // answer: require

//Q: - What Node.js module contains a method for server creation?
     // answer http module

//Q: - What is the method of the http object responsible for creating a server using Node.js?
    // answer:  .createServer() method


//Q: - What method of the response object allows us to set status codes and content types?
    // answer: response.writeHeader() method


//Q: - Where will console.log() output its contents when run in Node.js?
    // answer: server/ terminal

//Q: - What property of the request object contains the address's endpoint?
    //answer: request.url / url
