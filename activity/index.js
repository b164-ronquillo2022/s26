


const HTTP = require("http");
const port = 3000;

HTTP.createServer((request,response)=>{

    if (request.url ==`/login`){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end(`Welcome to the login page.`);
    } else {
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.end(`I'm sorry the page you are looking for cannot be found`);
    }

}).listen(port);

console.log(`Server successfully running${port}.`);
