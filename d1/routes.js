const http = require('http');

const port = 4000;
//creates a variable "server" that stores the output of the createServer method
const server = http.createServer((request,response) => {
    
   //to access the greeting (routes) we will use the request object
   //http://localhost:4000/greeting
   if(request.url == '/homepage'){
       response.writeHead(200, {'Content-Type': 'text/plain'});
       response.end('This is the Home Page! Welcome!');
   }
    if else (request.url == '/about'){
       response.writeHead(200, {'Content-Type': 'text/plain'});
       response.end('Welcome to about page!');
   }
     else (request.url == ''){
       response.writeHead(404, {'Content-Type': 'text/plain'});
       response.end('Page not found');

}
// use the server and port variable
server.listen(port);

//When server is running, print this:
console.log(`Server now accessible al localhost: ${port}.`);