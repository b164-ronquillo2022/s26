// create a basic server by loading NODE JS modules


//module it is a software component or part of a program that contains one or more routines

//http => hyper text transfer protocol
//HTTP is a module to node that transfer data. It is a set of individual files that contain code to create
//a component that helps established data transfer between applications
//Clients(browser) and servers (node js/express) communicate by exchanging
//individual messages
//The messeges sent by the client,usually a web browser .are called request.
// the messages sent by the server as an answer are called responses.




//require() function allows us to use built in modules
let http = require("http");
//A port is a virtual point where network connections start and end.
//Each port is associated with a specific process or service
// thes server will be asseigned to port 4000
//http://localhost:4000
http.createServer(function(request, response) {
   //use the writeHead method

         response.writeHead(200, {'Content-Type': 'application/json'});
         response.end("hello world");

}).listen(4000);

console.log('Server running on port 4000');

//status code
//100-199 => Informational responses
//200-299 => Successful responses
//300-399 => redirection messages
//400-499 => client error
//500 -599 => server eroor responses

//Type node index.js if we want to run/execute the code
//type cntrl C to stop/end the running of code

//nodemon
//it is a package that allow the server to automatically restart when files have been changed for update.
//This will allow us to hot reload the application meaning any changes applied to the application will be applied without having to restart it

//install
//npm install -g nodemon
//or sudo npm install nodemon -g

//to run using nodemon
//nodemon filename
//or npx nodemon filename
